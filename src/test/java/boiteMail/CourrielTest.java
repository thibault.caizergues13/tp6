package boiteMail;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import boiteMail.Courriel;

public class CourrielTest {
	
	Courriel courriel;
	//String adresseOk="juleslebg@umontpellier.fr";
	//String adresseNOk="marchepas";
	String titreOk="Titre de bg";
	String titreNOk=null;
	String messageOk="blablabla joint blablabla";
	String messageNOk="blablabla";
	ArrayList <String> pieceOk;
	ArrayList <String> pieceNOk;
	String pj="PJ.pdf";
	
	@BeforeEach
	public void setUp(){
		courriel = new Courriel();
		pieceOk = new ArrayList<String>();
		pieceNOk = new ArrayList<String>();
		pieceOk.add(pj);
	}
	
	/*@Test
	public void testAdresse() {
		assertEquals(true,courriel.VerifAdresse(adresseOk));
		assertEquals(false,courriel.VerifAdresse(adresseNOk));
	}*/
	
	@Test
	public void testTitre() {
		assertEquals(true,courriel.VerifTitre(titreOk));
		assertEquals(false,courriel.VerifTitre(titreNOk));
	}
	
	@Test
	public void testMessage() {
		assertEquals(true,courriel.VerifMessage(messageNOk,pieceOk));
		assertEquals(false,courriel.VerifMessage(messageOk,pieceNOk));
	}
	
	@DisplayName("validerAdresse")
		@ParameterizedTest
		@ValueSource(strings= {"juleslebg@umontpellier.fr","thibaultlepasboumontpellier.fr","cavaaa@oskour","oskour"})
		void testAdresse2 (String adresse) {
			assertEquals (true,courriel.VerifAdresse(adresse)) ;
		}
	

}
